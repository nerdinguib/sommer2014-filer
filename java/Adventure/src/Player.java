/**
 * Beskriver en spiller i et adventure spill.
 * Spilleren står i et rom og kan flytte seg
 * i en retning til et annet rom (nord,sør
 * vest,øst) avhengig av om rommet fører til
 * et nytt rom i den retningen.
 *
 */
public class Player{

	Room current; //Rommet spilleren står i

	//Når vi lager en ny spiller må vi si hvilket rom den står i
	public Player(Room current1){
		current = current1;
	}

	//finner ut (og printer ut) beskrivelsen av rommet spilleren står i
	public void writeDescription(){
		System.out.println("You are in the " + current.getDescription());
	}


	//beveger spilleren mot øst dersom det går an
	public void goEast(){
		if(current.hasRoomEast())
			current = current.getRoomEast();
		else
			System.out.println("There is no east room here");
	}

	//beveger spilleren mot vest dersom det går an
	public void goWest(){
		if(current.hasRoomWest())
			current = current.getRoomWest();
		else
			System.out.println("There is no west room here");
	}

	//beveger spilleren mot nord dersom det går an
	public void goNorth(){
		if(current.hasRoomNorth())
			current = current.getRoomNorth();
		else
			System.out.println("There is no north room here");
	}

	//beveger spilleren mot sør dersom det går an
	public void goSouth(){
		if(current.hasRoomSouth())
			current = current.getRoomSouth();
		else
			System.out.println("There is no south room here");
	}
}
