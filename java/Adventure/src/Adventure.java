import java.util.Scanner;

/**
 *
 * Et adventure spill der spilleren flytter seg rundt i rom som har forskjellig farge. 
 * Kartetover rom er som følger:
 * 
 *      N
 *      |            
 *   W - - E          ________
 *      |            |  red   |
 *      S    ________|--------|________
 *          | orange | yellow | green  |
 *          |________|________|________|
 *
 * Spilleren starter i det røde rommet.
 **/
public class Adventure {
	public static void main(String[] args){

		//Lar oss snakke med tastaturet
		Scanner keyboard = new Scanner(System.in);

		// Oppretter alle rommene
		Room redRoom = new Room("red room.");
		Room greenRoom = new Room("green room.");
		Room yellowRoom = new Room("yellow room.");
		Room orangeRoom = new Room("orange room.");

		// Kobler rommene sammen slik som på kartet
		redRoom.setRoomSouth(yellowRoom);
		yellowRoom.setRoomNorth(redRoom);
		yellowRoom.setRoomEast(greenRoom);
		greenRoom.setRoomWest(yellowRoom);
		yellowRoom.setRoomWest(orangeRoom);
		orangeRoom.setRoomEast(yellowRoom);

		// lar spilleren begynne i det røde rommet
		Player spiller = new Player(redRoom);

		//skriver ut til skjerm hvor spilleren er, og kommandoer
		spiller.writeDescription();
		System.out.println("Write \"go east\" for going to the east room, \nif there is one, and same for the other directions. \nWrite \"exit\" to exit game.");

		// Så lenge brukeren ikke skriver inn "exit" leses det inn nye kommandoer 
		boolean fortsett = true;
		String input;
		while(fortsett){
			input = keyboard.nextLine().toLowerCase();
			if(input.equals("go east")){
				spiller.goEast();
				spiller.writeDescription();
			}
			else if(input.equals("go west")){ 
				spiller.goWest();
				spiller.writeDescription();
			}
			else if(input.equals("go north")){
				spiller.goNorth();
				spiller.writeDescription();
			}
			else if(input.equals("go south")){
				spiller.goSouth();
				spiller.writeDescription();
			}
			else if(input.equals("exit")){
				System.out.println("Bye!");
				fortsett = false;
			}
			else{
				System.out.println("Invalid command, try again!");
			}
		}
	}
}
