/**
 * Beskrivelse av et rom i et adventure spill.
 * Et rom kan føre til andre rom i alle retninger
 * (nord,sør,vest,øst). 
 * Hvert rom har en beskrivelse (typisk en farge)
 *
 **/
public class Room{

  //Rom som dette rommet fører til.
  private Room roomWest;
  private Room roomEast;
  private Room roomNorth;
  private Room roomSouth;

  //beskrivelse av rommet
  private String description;

  //Når vi lager et nytt rom må vi gi det beskrivelse med én gang
  public Room(String description1){
    this.description = description1;
  }

  //bestemmer hvilket rom som skal ligge mot vest fra dette rommet
  public void setRoomWest(Room room){
    roomWest = room;
  }

  //bestemmer hvilket rom som skal ligge mot øst fra dette rommet
  public void setRoomEast(Room room){
    roomEast = room;
  }

  //bestemmer hvilket rom som skal ligge mot nord fra dette rommet
  public void setRoomNorth(Room room){
    roomNorth = room;
  }

  //bestemmer hvilket rom som skal ligge mot sør fra dette rommet
  public void setRoomSouth(Room room){
    roomSouth = room;
  }


  //finner ut hvilket rom som ligger mot vest fra dette rommet
  public Room getRoomWest(){
    return roomWest;
  }

  //finner ut hvilket rom som ligger mot øst fra dette rommet
  public Room getRoomEast(){
    return roomEast;
  }

  //finner ut hvilket rom som ligger mot nord fra dette rommet
  public Room getRoomNorth(){
    return roomNorth;
  }

  //finner ut hvilket rom som ligger mot vest fra dette rommet
  public Room getRoomSouth(){
    return roomSouth;
  }

  //finner ut om denne rommet har et rom mot vest
  public boolean hasRoomWest(){
    return roomWest != null;
  }

  //finner ut om denne rommet har et rom mot øst
  public boolean hasRoomEast(){
    return roomEast != null;
  }

  //finner ut om denne rommet har et rom mot nord
  public boolean hasRoomNorth(){
    return roomNorth != null;
  }

  //finner ut om denne rommet har et rom mot sør
  public boolean hasRoomSouth(){
    return roomSouth != null;
  }

  //finner ut hva beskrivelsen av rommet er
  public String getDescription(){
    return description;
  }
}
