/**
 * En 2D-tabell av Kort 
 * 
 */
public class Tabell2D{
	private Kort[][] tabell;
	private int antX;
	private int antY;

	//Når vi lager en ny 2DTabell må vi vite lengde og bredde
	Tabell2D(int antx, int anty){
		tabell = new Kort[antx][anty];
		antX=antx;
		antY=anty;
	}

	//henter kortet på plass x, y
	public Kort hent(int x, int y) {
		return tabell[x][y];
	}

	//legger et kort på plass x, y
	public void sett(int x, int y, Kort kort) {
		tabell[x][y] = kort;
	}

	//finner ut høyden på tabellen
	public int høyde() {
		return antY;
	}

	//finner ut bredden på tabellen
	public int bredde() {
		return antX;
	}

	//finner ut antall kort som kan ligge i tabellen
	public int antall(){
		return antY*antX;
	}

	//kloner tabellen slik at vi får to like tabeller med like kort etterpå 
	public Tabell2D clone(){
		Tabell2D klone = new Tabell2D(antX, antY);
		for(int i = 0; i<antX; i++){
			for(int j = 0; j<antY; j++){
				klone.sett(i, j, tabell[i][j]);
			}
		}
		return klone;
	}
}
