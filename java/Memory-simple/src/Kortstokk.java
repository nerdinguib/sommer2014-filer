import java.util.Random;

/**
 * Kortstokken vår, bestående av kort-par 
 * 
 */
public class Kortstokk {

	private static Kort[] samling;		// samling av like kort


	//Vi forteller hvor mange kort vi trenger, gir denne metoden oss en shufflet, passelig 
	//stor samling av par av kort fra kortstokken
	public static Kort[] hentKort(int antall){
		Kort[] brett = new Kort[antall];
		for (int i=0; i<antall; i++)
			brett[i]=samling[i];
		lukkAlle(brett);

		//shuffle
		Random r = new Random();
		for(int k=0; k<antall; k++){
			Kort tmp = brett[k];
			int pos= k + r.nextInt(antall-k);
			brett[k]=brett[pos];
			brett[pos]=tmp;
		}
		return brett;
	}

	//snur alle kortene slik at baksiden er opp (kan tenkes på som å "lukke" dem)
	private static void lukkAlle(Kort[] brett){
		for (int j=0; j<brett.length; j++) {
			brett[j].lukk(); }
	}

	//"lager" kortstokken vår
	public static void nyKortstokk(){
		samling = new Kort[Vanskelighet.XLARGE.antall()];
		int i=0;
		samling[i++] = new Kort("umbrella2.jpg");
		samling[i++] = new Kort("umbrella2.jpg");
		samling[i++] = new Kort("lilly2.jpg");
		samling[i++] = new Kort("lilly2.jpg");
		samling[i++] = new Kort("urban.jpg");
		samling[i++] = new Kort("urban.jpg");
		samling[i++] = new Kort("feather.jpg");
		samling[i++] = new Kort("feather.jpg");
		samling[i++] = new Kort("tree.jpg");
		samling[i++] = new Kort("tree.jpg");
		samling[i++] = new Kort("fing.jpg");
		samling[i++] = new Kort("fing.jpg");
		samling[i++] = new Kort("up.jpg");
		samling[i++] = new Kort("up.jpg");
		samling[i++] = new Kort("paris2.jpg");
		samling[i++] = new Kort("paris2.jpg");
		samling[i++] = new Kort("reddit.jpg");
		samling[i++] = new Kort("reddit.jpg");
		samling[i++] = new Kort("face.jpg");
		samling[i++] = new Kort("face.jpg");
		samling[i++] = new Kort("eye.jpg");
		samling[i++] = new Kort("eye.jpg");
		samling[i++] = new Kort("dali2.jpg");
		samling[i++] = new Kort("dali2.jpg");
		samling[i++] = new Kort("ohno.jpg");
		samling[i++] = new Kort("ohno.jpg");
		samling[i++] = new Kort("htc.jpg");
		samling[i++] = new Kort("htc.jpg");
		samling[i++] = new Kort("tux.jpg");
		samling[i++] = new Kort("tux.jpg");
		samling[i++] = new Kort("batman.jpg");
		samling[i++] = new Kort("batman.jpg");
		samling[i++] = new Kort("mus.jpg");
		samling[i++] = new Kort("mus.jpg");
		samling[i++] = new Kort("apple.jpg");
		samling[i++] = new Kort("apple.jpg");
		samling[i++] = new Kort("bug.jpg");
		samling[i++] = new Kort("bug.jpg");
		samling[i++] = new Kort("android2.jpg");
		samling[i++] = new Kort("android2.jpg");
		samling[i++] = new Kort("owl2.jpg");
		samling[i++] = new Kort("owl2.jpg");
		samling[i++] = new Kort("jack2.jpg");
		samling[i++] = new Kort("jack2.jpg");
		samling[i++] = new Kort("puff2.jpg");
		samling[i++] = new Kort("puff2.jpg");
		samling[i++] = new Kort("foto.jpg");
		samling[i++] = new Kort("foto.jpg");
		samling[i++] = new Kort("lava.jpg");
		samling[i++] = new Kort("lava.jpg");
		samling[i++] = new Kort("ape.jpg");
		samling[i++] = new Kort("ape.jpg");
		samling[i++] = new Kort("box.jpg");
		samling[i++] = new Kort("box.jpg");
	}
}
