	import java.awt.Insets;
	
	import javax.swing.ImageIcon;
	import javax.swing.JButton;
	
	/**
	 * Klasse som får Kort-klassen til å snakke fint med GUI-klassen
	 *
	 */
	
	public class GUIKort extends JButton {
		private static final long serialVersionUID = -7919110561211386669L;
		private ImageIcon åpen;
		private ImageIcon lukket;
		Kort kort;
	
		public GUIKort(Kort kort, String lukketBilde) {
			super();
			this.kort = kort;
			setContentAreaFilled(false);
	 		åpen = new ImageIcon(getClass().getResource(GUI.bildeSti + kort.bilde()));
			lukket = new ImageIcon(getClass().getResource(GUI.bildeSti + lukketBilde));
			oppdater();
			setMargin(new Insets(0,0,0,0));
		}
		public boolean oppdater() {
			if(isVisible()) {
				if(kort.erÅpen())
					setIcon(åpen);
				else
					setIcon(lukket);
			
				if(!isEnabled())
					setVisible(false);
				
				if(!kort.erAktiv()) {
					setEnabled(false);
					this.setDisabledIcon(åpen);
					return true;
				}
			}
			return false;
		}
	}
