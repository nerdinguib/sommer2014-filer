/**
 * Tilstander for Brikke-klassen. 
 *
 */

public enum Tilstand {LUKKET, ÅPEN, FJERNET}