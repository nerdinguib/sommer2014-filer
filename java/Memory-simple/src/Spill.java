/**
 * Selve memory-spillet.
 *  
 */
public class Spill{

	private Tabell2D brett;
	private int ant;			// Oversikt over antall aktive kort, altså kort som ikke er fjernet
	private long startTid;

	// Initialiserer de aktive brikkene
	private Kort kort1=null;
	private Kort kort2=null;
	
	// Endre evt variabler her: 
	private String bakgrunnsbilde = "bakgrunn3.jpg";
	private String vunnetbilde = "winning.jpg";

	public static void main(String[] args){
		Spill spill = new Spill();
		Kortstokk.nyKortstokk();
		new GUI(spill);
	}

	//Lager et nytt spill (vi må bestemme vanskelighet) og fyller ut en passelig stor 2D-Tabell
	//med kort som er hentet fra den shufflede kortstokken vår 
	public Tabell2D nyttSpill(Vanskelighet v) {
		brett = new Tabell2D(v.bredde(), v.høyde());
		ant=v.antall();
		Kort[] blandedeKort = Kortstokk.hentKort(ant);
		for (int j=0, k=0; j<v.bredde(); j++){
			for (int h=0; h<v.høyde(); h++, k++){
				brett.sett(j, h, blandedeKort[k]);
			}
		}
		startTid=System.currentTimeMillis();
		return brett;
	}

	//bestemmer hva som skal skje med kortet som ble trykket på (kortet på plass x, y i tabellen)
	public boolean velg(int x, int y){
		//Enten har vi to "snudde" kort
		if(!(kort2==null)){
			kort1.lukk();
			kort2.lukk();
			kort1=null;
			kort2=null;
		}
		//ellers har vi ingen snudde kort og må snu det første
		if (kort1==null && kort2==null){
			kort1 = brett.hent(x,y);
			brett.hent(x,y).åpne();
			return true;
		}
		//ellers har vi ett snudd kort og må snu et til
		if(kort1!=null){
			kort2=brett.hent(x, y);
			kort2.åpne();
			if (kort1.erLik(kort2)){
				kort1.fjern();
				kort2.fjern();
				ant-=2;
				if (ant==0)
					return false;
				return true;
			}
		}return true;
	}

	//finner ut hvor mange aktive kort som er i tabellen
	public int antall() {
		return ant;
	}

	//finner ut hvor lenge vi har spilt
	public int tid() {
		return (int)((System.currentTimeMillis()-startTid)/1000);
	}

	//finner ut hva som er bakgrunnsbilde på spillet
	public String bakgrunnsBilde() {
		return bakgrunnsbilde;
	}

	//finner ut hvilket bilde som skal komme opp når vi har vunnet
	public String vunnetBilde(){
		return vunnetbilde;
	}

	//finner ut hvilken tekst som skal komme opp når vi har vunnet
	public String sluttTekst() {
		double tid = (System.currentTimeMillis()-startTid)/1000;
		String bruktTid = "" +  (int) tid/60 + " min " + tid% 60 + " sek"; 
		return "Hurra, du vant et bilde! Du brukte " + bruktTid + " på å fullføre.";
	}
}