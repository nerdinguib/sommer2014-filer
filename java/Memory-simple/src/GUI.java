	import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;
	
	
	@SuppressWarnings("serial")
	public class GUI extends JFrame implements ActionListener {
	
		/**
		 * Her ligger spillebrikkene
		 */
		private ImagePanel hovedPanel;
		/**
		 * Viser tid og poeng nederst
		 */
		private JPanel statusPanel;
		/**
		 * 'Nytt spill' knapp og størrelsesvalg
		 */
		private JPanel kontrollPanel;
		/**
		 *  Knapper for å starte nytt spill
		 */
		private JButton nytt, igjen;
		/**
		 * Størrelsesvalg
		 */
		private JRadioButton small, medium, large, xlarge;
		/**
		 * Vise tid og antall brikker
		 */
		private JLabel antall, tid;
		/**
		 * For å finne X og Y til knappen som ble trykket
		 */
		private Map<Object, Integer> xMap;
		private Map<Object, Integer> yMap;
		/**
		 * Liste over alle aktive brikker
		 */
		private ArrayList<GUIKort> kortstokk;
		/**
		 * Referanse til spillet
		 */
		private Spill spill;
		/**
		 * Vekker oss hvert halve sekund
		 */
		private javax.swing.Timer timer;
	
		/**
		 * Katalog hvor GUI-en leter etter bilder (relativt til hvor klassefilene ligger)
		 */
		public static String bildeSti = "images/";
	
		/**
		 * Oppretter en ny spill-GUI
		 * @param spill2 Spillet som skal kontrolleres
		 */
		public GUI(Spill spill2) {
			super("Memo");
	
			this.spill = spill2;
			timer = new javax.swing.Timer(500, this);  // vekk oss hvert 500 millisekund
	
			kontrollPanel = new JPanel();
			hovedPanel = new ImagePanel(bildeSti + spill2.bakgrunnsBilde());
			statusPanel = new JPanel();
	
			add(kontrollPanel, BorderLayout.NORTH);		
			add(hovedPanel,BorderLayout.CENTER);
			add(statusPanel, BorderLayout.SOUTH);
	
			nytt = new JButton("Nytt Spill");
			nytt.addActionListener(this);
			small = new JRadioButton("S", false);
			medium = new JRadioButton("M", true);
			large = new JRadioButton("L", false);
			xlarge = new JRadioButton("XL", false);
	
			ButtonGroup g = new ButtonGroup();
			g.add(small);
			g.add(medium);
			g.add(large);
			g.add(xlarge);
	
			kontrollPanel.add(nytt);
			kontrollPanel.add(new JLabel("Brettstørrelse:"));
			kontrollPanel.add(small);
			kontrollPanel.add(medium);
			kontrollPanel.add(large);
			kontrollPanel.add(xlarge);
			kontrollPanel.add(new JLabel("  "));
			
			antall = new JLabel("Brikker: 0");
			tid = new JLabel("Tid: 0:00");
			statusPanel.add(antall);
			statusPanel.add(tid);
	
			lagBrett(spill2.nyttSpill(Vanskelighet.MEDIUM));
	
			pack();
			setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		}
	
		/**
		 * Lager et nytt spillbrett på hovedpanelet
		 * 
		 * @param brett En 2D-tabell med brikker
		 */
		private void lagBrett(Tabell2D brett) {
			xMap = new HashMap<Object, Integer>();
			yMap = new HashMap<Object, Integer>();
			kortstokk = new ArrayList<GUIKort>();
			hovedPanel.removeAll();
			hovedPanel.setLayout(new GridLayout(brett.høyde(), brett.bredde()));
			for(int x = 0; x < brett.bredde(); x++)
				for(int y = 0; y < brett.høyde(); y++) {
					JPanel p = new JPanel();
					GUIKort b = new GUIKort(brett.hent(x, y),  "bakside.jpg");
					kortstokk.add(b);
					b.addActionListener(this);
					p.add(b);
					p.setOpaque(false);
					hovedPanel.add(p);
					xMap.put(b, x);
					yMap.put(b, y);
				}
			timer.start();
			hovedPanel.settBilde(bildeSti +  spill.bakgrunnsBilde());
			pack();
		}
	
		/**
		 * Går gjennom og oppdaterer grafikken til alle brikkene, og viser oppdatert informasjon i displayet
		 */
		private void oppdater() {
			boolean delay = false;
			for(GUIKort k : kortstokk)
				delay |= k.oppdater();
			if(delay)
				timer.start();
			antall.setText(String.format("Brikker: %d", spill.antall()));
			tid.setText(String.format("Tid: %d:%02d", spill.tid()/60, spill.tid()%60));
			hovedPanel.settBilde(bildeSti +  spill.bakgrunnsBilde());
	
		}
		/** 
		 * Denne blir kalt av Java hver gang brukeren trykker på en knapp, eller hver gang
		 * timer-signalet avfyres.
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == nytt || e.getSource() == igjen) {
				Vanskelighet v = Vanskelighet.MEDIUM;
				if(small.isSelected())
					v = Vanskelighet.SMALL;
				else if(medium.isSelected())
					v = Vanskelighet.MEDIUM;
				else if(large.isSelected()) 
					v = Vanskelighet.LARGE;
				else if(xlarge.isSelected())
					v = Vanskelighet.XLARGE;
				lagBrett(spill.nyttSpill(v));
			}
			else if(e.getSource() == timer) {
				//timer.stop();
				oppdater();
			}
			else {
				int x = xMap.get(e.getSource());
				int y = yMap.get(e.getSource());
	
				if(spill.velg(x, y))
					oppdater();
				else{
					vunnet();
				}
			}
		}
	
		/**
		 * Vis vinnertekst og be brukeren spille en gang til 
		 */
		private void vunnet() {
			hovedPanel.settBilde(bildeSti + spill.vunnetBilde());
			hovedPanel.removeAll();
			hovedPanel.setLayout(new GridLayout(4,1));
			JPanel p = new JPanel();
			p.setOpaque(false);
			igjen = new JButton("Nytt spill?");
			igjen.addActionListener(this);
			igjen.setOpaque(false);
			p.add(igjen);
			JLabel l = new JLabel(spill.sluttTekst());
			l.setHorizontalAlignment(SwingConstants.CENTER);
			l.setForeground(Color.WHITE);
			l.setOpaque(false);
			JPanel p1 = new JPanel();
			p1.setOpaque(false);
			JPanel p2 = new JPanel();
			p2.setOpaque(false);
			hovedPanel.add(p1);
			hovedPanel.add(l);
			hovedPanel.add(p);
			hovedPanel.add(p2);
			
			//Setter inn antall... for å løse brikker=0 ved vunnet spill
			antall.setText(String.format("Brikker: %d", spill.antall()));
			timer.stop();
			validate();
		}
	
	}
	@SuppressWarnings("serial")
	class ImagePanel extends JPanel {
	
		private Image img;
		private String imgNavn;
		private int w;
		private int h;
	
		public ImagePanel(String bilde) {
			super();
			settBilde(bilde);
			Dimension size = new Dimension(w, h);
			setSize(size);
		}
		public void settBilde(String bilde) {
			if(!bilde.equals(imgNavn)) {
				img =  new ImageIcon(getClass().getResource(bilde)).getImage();
				w = img.getWidth(null);
				h = img.getHeight(null);
				imgNavn = bilde;
				validate();
			}
		}
	
		public void paintComponent(Graphics g) {
			int gw = this.getWidth();
			int gh = this.getHeight();
			int iw = w;
			int ih = h;
			int ix = 0, iy = 0;
			if(iw-gw > 20 || ih-gh > 20) {
				if((double)gw/gh > (double)iw/ih) {
					ih = (int)((double)iw*(double)gh/(double)gw);
					iy = (h-ih)/2;
				}
				else {
					iw = (int)((double)ih*(double)gw/(double)gh);
					ix = (w-iw)/2;
				}
			}
			else {
				if(iw > gw) {
					ix = (gw-iw)/2;
					iw = gw;
				}
				if(ih > gh) {
					iy = (gh-ih)/2;
					ih = gh;
				}
			}
			g.drawImage(img, 0, 0, gw-1, gh-1, ix, iy, iw-1, ih-1, null);
		}
	
	}