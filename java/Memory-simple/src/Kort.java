/**
 * Kort til bruk i kortstokken. Hvert kort har et bilde og en "tilstand": åpen, lukket, fjernet 
 * tilsvarende om bildet vises eller ikke, eller om kortet er tatt vekk fra brettet. 
 * 
 * Bildet er gitt ved en navnet til bildet.
 * 
 */
public class Kort{
	private Tilstand tilstand=Tilstand.LUKKET;
	private String bilde;
	
	//når vi lager et nytt kort må vi si hvilket bilde det skal ha
	public Kort(String BILDE){
		bilde=BILDE;		
	}
	
	//finner ut om et kort er snudd, altså åpent (bildet vises)
	public boolean erÅpen() {
		return tilstand==Tilstand.ÅPEN;
	}

	//finner ut om et kort er i spill, det vil si ikke fjernet fra brettet 
	public boolean erAktiv() {
		return tilstand!=Tilstand.FJERNET;
	}

	//lukker, eller snur kortet så bildet ikke vises
	public void lukk() {
		tilstand = Tilstand.LUKKET;
	}

	//åpener, eller snus kortet så bildet vises
	public void åpne() {
		tilstand = Tilstand.ÅPEN;
	}

	//fjerner kortet fra spillet
	public void fjern() {
		tilstand=Tilstand.FJERNET;
	}

	//finner ut hva navnet til bildet på kortet er
	public String bilde() {
		return bilde;
	}

	//finner ut om to kort er like (har samme bilde)
	public boolean erLik(Kort b) {
		if (this==b || this==null || b==null)
			return false;
		return this.bilde()==b.bilde();
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kort other = (Kort) obj;
		if (bilde != null)
			if (bilde.equals(other.bilde))
				return true;
		return false;
	}
	
	//kloner et kort, lager et nytt kort som er likt 
	public Kort clone(){
		Kort nyBrikke = new Kort(this.bilde);
		nyBrikke.tilstand = this.tilstand;
		return nyBrikke;
	}
}