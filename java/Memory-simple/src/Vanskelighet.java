
/** 
 * Dette representerer vanskeligheten på et spill, vanskeligheten tilsvarer 
 * dimensjoner av brettet:  
 * 
 * SMALL tilsvarer 3*6
 * MEDIUM tilsvarer 4*7
 * LARGE tilsvarer 5*8
 * XLARGE tilsvarer 6*9
 * 
 */

public enum Vanskelighet {
	SMALL,
	MEDIUM,
	LARGE,
	XLARGE;
	
	//finner ut hva bredden til et spill med en viss vanskelighet er 
	public int bredde() {
		switch(this) {
		case SMALL:
			return 6;
		case MEDIUM:
			return 7;
		case LARGE:
			return 8;
		case XLARGE:
			return 9;
		}
		return 0; //Dette skal aldri skje
	}
	
	//finner ut hva høyden til et spill med en viss vanskelighet er 
	public int høyde() {
		switch(this) {
		case SMALL:
			return 3;
		case MEDIUM:
			return 4;
		case LARGE:
			return 5;
		case XLARGE:
			return 6;
		}
		return 0; //Dette skal aldri skje
	}
	
	//finner ut hvor mange kort vi kan ha i et spill med en viss vanskelighet
	public int antall() {
		return bredde() * høyde();
	}
}
