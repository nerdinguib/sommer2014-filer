%%
%% Defines listing environments for Magnolia and various other programming languages.
%%
%% See generic code language  (CodeLanguage) below for language setup and command examples.
%% The following commands/environments format code according to the language definition,
%% where the phrase ``code'' should be replaced by the language you want to use.
%% Recommended:
%% - \lcode{..} inline code formatting 
%% - \begin{code}..\end{code} multi-line code environment 
%% Overview, inline code versions
%% - \bcode!xx! or \bcode{xx} basic lstinline code
%%   no background colour, highlights according to language definition, handles utf-8, cannot be used in math mode
%% - \bcodebr!xx! or \bcodebr{xx} basic lstinline code with line breaking
%%   no background colour, highlights according to language definition, handles utf-8, cannot be used in math mode, does line breaking
%% - \lcode{xx} lstinline code
%%   background colour, highlights according to language definition, handles some utf-8, can be used in math mode
%% - \mcode{xx} math code
%%   background colour, no highlighting, handles most utf-8, does recognise latex (math) commands, can be used in math mode
%% Overview, multi-line code environment versions
%% - \begin{code}..\end{code} multi-line code environment
%%   background colour, line numbers, highlights according to language definition, handles utf-8
%% - \begin{codehl}..\end{codehl} multi-line code environment with additional highlighting
%%   background colour, line numbers, highlights according to language definition, handles utf-8, selectively colour text between _| xx |_
%%
%% Assumes the listings and the color packages already have been imported, use as follows
%% \usepackage{color} % colour in listings
%% \usepackage{listings}
%% \usepackage{magnolia} % Defines various useful languages
%%
%% @author Magne Haveraaen, Tero Hasu, Anya Bagge
%% @revised 2013-08-27

%% Colouring scheme
\definecolor{codebackground}{rgb}{0.98,0.98,0.98}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{keyword}{rgb}{0.5,0.0,0.5}
\definecolor{emph}{rgb}{0.3,0.0,0.0}
\definecolor{string}{rgb}{0.5,0.3,0.2}
\definecolor{comment}{rgb}{0.3,0.5,0.3}
\definecolor{code}{rgb}{0.0,0.0,0.3}
\definecolor{static}{rgb}{0.0,0.0,0.3}
\definecolor{highlight}{rgb}{1.0,0.0,0.3}
%% Feel free to redefine colours
% \definecolor{codebackground}{rgb}{0.5,0.6,0.7}


%% Load some relevant base languages
%\lstloadlanguages{Java, Lisp, XML, Pascal}


%% Setting up formatting options across languages
\lstset{%
  extendedchars=\true,inputencoding=utf8x,% allows (rudimentary) use of utf-8
  fontadjust=true,%
  showstringspaces=false,%
  basicstyle=\color{code}\small,
  keywordstyle=\color{keyword}\bf,%\underbar,%
  commentstyle=\color{comment}\sl,%\rmfamily\itshape\footnotesize,% alternate: commentstyle=\sl\color{black},
  stringstyle=\color{string}\rm,%\itshape,%
  emphstyle=\color{emph}\sl,%\slshape,%
  % moredelim=[is][\color{highlight}]{|_}{|_}, % Does not work from here, intention is that everything between these will be highlighted.
  columns=flexible,%
  backgroundcolor=\color{codebackground},
  %escapeinside={\%*}{*)},
  escapeinside={($}{$)},
  tabsize=4,
  aboveskip=1.2\smallskipamount,%
  belowskip=1.2\smallskipamount,%
  % texcl=true, mathescape=true,%
}
%% Special characters, not needed in the UTF-8 setting.
% \lstset{
% literate={∀}{\csname u-default-8704\endcsname}2
% {·}{\csname u-default-8226\endcsname}2
% {≤}{\csname u-default-8804\endcsname}2
% }
%% Inline setup
\newcommand{\lstinlinedefaults}{\lstset{%
  numbers=none                   % where to put the line-numbers: left none
}}
%% Line breaking in inline setup
\newcommand{\lstinlinebreaks}{\lstinlinedefaults\lstset{%
  breaklines=true,breakatwhitespace=true,literate={\_}{}{0\discretionary{\_}{}{\_}}{\\\-}{}{0\discretionary{-}{}{}}
}}
%% Environment setup
\newcommand{\lstenvironmentdefaults}{\lstset{%
  numbers=none,                   % where to put the line-numbers: left none
  numberstyle=\small\color{gray}, % the style that is used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers
  numbersep=3pt,                  % how far the line-numbers are from the code
}}

%% Some additional inline breaking commands, language needs to be selected in surrounding context.
\newcommand\lstinlinebra{\lstset{breaklines=true,breakatwhitespace=false}\lstinline} % break anywhere
\newcommand\lstinlinebrh{\lstset{breaklines=true,breakatwhitespace=true,literate={\-}{}{0\discretionary{-}{\\}{}}}\lstinline} % break at hyphen
\newcommand\lstinlinehyp{\lstset{literate={\\\-}{}{0\discretionary{-}{}{}}}\lstinline}

%% These do not seem to add any specific functionality.
\def\highlightbox#1{\setlength{\fboxsep}{0pt}\colorbox{yellow}{\strut#1}}
\lstdefinestyle{highlightstyle}{moredelim=[is][\highlightbox]{[|}{|]}}

\def\codebox#1{\setlength{\fboxsep}{0pt}\colorbox{codebackground}{\strut#1}}


%% Some generic code language with invariants and assertíons for demonstration purposes.
\lstdefinelanguage{CodeLanguage}[ISO]{C++}{% Using standard C++ as the base language
  morekeywords={% Additional keywords not in the base language
    concept,implication,satisfy,theorem,by,qed,axiom,requires,%
    assert,assertPre,assertPost,classInvariant,assert_axiom,assert_induction,base,step,assertDecl,%
    procedure,boolean,super,likeThis,old,%
    types,interface,opns%
  },
% %    otherkeywords={->,:},%
% %    moredelim=[is][\color{red}]{|}{|},
}
\newcommand{\bcode}{\lstinline}% Deals with all symbols, does not colour the background, cannot be used in math mode.
\newcommand{\bcodebr}{\lstinlinebreaks\lstset{language=CodeLanguage}\lstinline}% As \bcode, breaks lines if they are too long.
\newcommand{\lcode}[1]{\codebox{\lstinline{#1}}} % Colours the background, some UTF-8 math symbols, no trailing spaces in argument.
\newcommand{\mcode}[1]{\colorbox{codebackground}{\ensuremath{\mathtt{#1}}}}% Colours the background, no highlighting, distorts some UTF-8 math symbols, does math.
\lstnewenvironment{code}{\lstenvironmentdefaults\lstset{language=CodeLanguage}}{}% Environment: Deals with all symbols, colours background, numbers the lines.
\lstnewenvironment{codehl}{\lstenvironmentdefaults\lstset{language=CodeLanguage,moredelim=[is][\color{highlight}]{_|}{|_}}}{}% Environment as above, highlights between _| and |_




%% Magnolia
\lstdefinelanguage{Magnolia}{%
  keywords={%
    package,imports,%
    program,implementation,signature,concept,satisfaction,models,morphism,protect,%
    +,++,@,@@,**,=>,%&&,||,
    times,union,retain,remove,hide,reveal,requires,%
    require,requires,approximates,extend,defines,external,with,on,by,
    use,value,alert,alerts,leave,leaves,precond,throw,throws,handler,unless,pre,post,invariant,assert,throwing,%
    predicate,procedure,function,axiom,theorem,proof,class,type,struct,default,override,guard,%
    call,const,var,assert,open,new,preserve,generate,free,initial,partition,renaming,homomorphism,%
    datainvariant,subalgebra,congruence,quotient,full,declared,%
    rule,transform,forall,exists,name,skip,FALSE,TRUE,recover,%
    obs,upd,out,del,giv,nrm,expr,%
    break,case,continue,default,do,else,for,if,then,end,let,return,switch,while,repeat,along,from,in%
  },%
  sensitive,%
  morecomment=[s]{/*}{*/},%
  morecomment=[l]//,% nonstandard
  morestring=[b]",%
  literate={\\\$}{\$}{1},%
}[keywords,comments,strings]%
\newcommand{\bmagnolia}{\lstset{language=Magnolia,}\lstinline}% Deals with all symbols, does not colour the background, cannot be used in math mode.
\newcommand{\bmagnoliabr}{\lstinlinebreaks\lstset{language=Magnolia}\lstinline}% As \bcode, breaks lines if they are too long.
\newcommand{\lmagnolia}[1]{\lstset{language=Magnolia,}\colorbox{codebackground}{\lstinline{#1}}}% Colours the background, some UTF-8 math symbols, no trailing spaces in argument.
\newcommand{\mmagnolia}[1]{\colorbox{codebackground}{\ensuremath{\mathtt{#1}}}}% Colours the background, no highlighting, distorts some UTF-8 math symbols, does math.
\lstnewenvironment{magnolia}{\lstenvironmentdefaults\lstset{language=Magnolia}}{}
\lstnewenvironment{magnoliahl}{\lstenvironmentdefaults\lstset{language=Magnolia,moredelim=[is][\color{highlight}]{_|}{|_}}}{}
% Short versions, not according to current standard
\newcommand{\magno}{\bmagnolia}
\newcommand{\magnobr}{\bmagnoliabr}


%% C++ language
\newcommand{\bcxx}{\lstset{language=C++}\lstinline}
\newcommand{\lcxx}[1]{\lstset{language=C++,}\colorbox{codebackground}{\lstinline{#1}}}% Colours the background, some UTF-8 math symbols, no trailing spaces in argument.
\lstnewenvironment{cxx}{\lstset{language=C++}}{}
\lstnewenvironment{cxxhl}{\lstenvironmentdefaults\lstset{language=C++,moredelim=[is][\color{highlight}]{_|}{|_}}}{}
\lstnewenvironment{halfcpphl}{\begin{minipage}{.45\linewidth}\small\lstset{language=C++, style=highlightstyle}}{\end{minipage}}

%% Java
\newcommand{\bjava}{\lstset{language=Java}\lstinline}
\newcommand\bjavabr{\lstinlinebreaks\lstset{language=Java}\lstinline}
\lstnewenvironment{java}{\lstenvironmentdefaults\lstset{language=Java}}{}
\lstnewenvironment{javahl}{\lstenvironmentdefaults\lstset{language=Java,moredelim=[is][\color{highlight}]{_|}{|_}}}{}

%% JAxT, a concept extension to Java
\lstdefinelanguage{ConceptJava}[]{Java}%
  {moredelim=[is][\color{red}]{|}{|}}
\newcommand{\ljaxt}[1]{\lstset{language=ConceptJava}\colorbox{codebackground}{\lstinline{#1}}}
\lstnewenvironment{jaxt}{\lstenvironmentdefaults\lstset{language=ConceptJava}}{}

%% ML
\newcommand{\bml}{\lstset{language=ML}\lstinline}

%% PGF
\lstdefinelanguage[]{PGF}{
  keywords={insert,append,delete,break,mode,push.m,pop.m,table,push.t,pop.t,category,nop,ignore,@},%
  otherkeywords={=>,[,],<.,<:},%
  morecomment=[s]{/*}{*/},%
  morecomment=[l]//,% nonstandard
  morestring=[b]",%
  morestring=[b]',%
  sensitive,%
  alsoletter={.=>},%
}[keywords,comments,strings]
\newcommand{\bpgf}{\lstset{language=PGF,}\lstinline}% Deals with all symbols, does not colour the background, cannot be used in math mode.
\newcommand{\bpgfbr}{\lstinlinebreaks\lstset{language=PGF}\lstinline}% As \bcode, breaks lines if they are too long.
\newcommand{\lpgf}[1]{\lstset{language=PGF,}\colorbox{codebackground}{\lstinline{#1}}}% Colours the background, some UTF-8 math symbols, no trailing spaces in argument.
\newcommand{\mpgf}[1]{\colorbox{codebackground}{\ensuremath{\mathtt{#1}}}}% Colours the background, no highlighting, distorts some UTF-8 math symbols, does math.
\lstnewenvironment{pgf}{\lstenvironmentdefaults\lstset{language=PGF}}{}% Environment: Deals with all symbols, colours background, numbers the lines.
\lstnewenvironment{pgfhl}{\lstenvironmentdefaults\lstset{language=PGF,moredelim=[is][\color{highlight}]{_|}{|_}}}{}% Environment as above, highlights between _| and |_

%% Racket
\lstdefinelanguage{Racket}[]{Lisp}{deletekeywords={continue, eq, equal, export, fill, get, rest, set, some, string}, morekeywords={begin, call-with-continuation-prompt, car, cdr, compose, curry, curryr, define, define-for-syntax, define-generics, define-match-expander, define-syntax, define-syntax-rule, define-syntaxes, define-values, else, eq?, eqv?, equal?, filter, for-syntax, for/list, format-id, generate-temporary, hash-ref, hash-set, if, lambda, lazy, let-syntax, let-values, letrec, make-rename-transformer, match, negate, null?, number?, ormap, pair?, \#\%plain-lambda, promise?, set!, string?, struct, struct-copy, syntax/loc, syntax-case, syntax-e, syntax-id-rules, syntax-local-eval, syntax-local-value, syntax-rules, thunk, unsafe-struct-ref, values, void, with-syntax}, alsoletter={\#}, alsodigit={?, !, \$, \%, &, *, +, -, ., /, :, <, =, >, @, ^, _, ~}, morecomment=[s]{\#|}{|\#}}[keywords,comments,strings]
\lstnewenvironment{racket}{\lstenvironmentdefaults\lstset{language=Racket}}{}
\newcommand{\bracket}{\lstset{language=Racket}\lstinline}
\newcommand{\lracket}[1]{\lstset{language=Racket}\codebox{\lstinline{#1}}}

%% Rascal
\lstdefinelanguage{Rascal}[]{Java}{morekeywords={data,lexical,str,syntax}}[keywords,comments,strings]

%% Stratego
\lstdefinelanguage{Stratego}[]{Java}{morekeywords={one,some,all}}[keywords,comments,strings]
\newcommand{\bstratego}{\lstset{language=Stratego}\lstinline}
\newcommand{\lstratego}[1]{\lstset{language=Stratego}\codebox{\lstinline{#1}}}

%% Scheme
\lstdefinelanguage{Scheme}[]{Lisp}{deletekeywords={continue}, morekeywords={define, letrec, values}}[keywords,comments,strings]
\newcommand{\bscheme}{\lstset{language=Scheme}\lstinline}

%% Defining a specification language spec/ConceptPascal
\lstdefinelanguage{ConceptPascal}[Standard]{Pascal}%
  {morekeywords={interface,specification,struct,type,types,opn,opns,var,axiom,axioms,TRUE,FALSE},otherkeywords={->,:},moredelim=[is][\color{red}]{|}{|}}
\newcommand{\lspec}[1]{\lstset{language=ConceptPascal}\colorbox{codebackground}{\lstinline{#1}}}
\lstnewenvironment{spec}{\lstset{language=ConceptPascal}}{}

\newcommand\javabr{\lstset{language=Java,breaklines=true,breakatwhitespace=true,literate={\_}{}{0\discretionary{\_}{}{\_}}{\\\-}{}{0\discretionary{-}{}{}}}\lstinline}


%% Setting up some lst inline formatting rule. This is context sensitive.
% \newcommand{\myverb}[1]{\lstset{basicstyle=\ttfamily}\lstinline{#1}}
% \newcommand\myverb{\lstset{basicstyle=\ttfamily}\lstinline}
\newcommand\myverb{\lstset{basicstyle=\rmfamily}\lstinline}

