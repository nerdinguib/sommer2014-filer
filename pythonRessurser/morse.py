kort = 0
lang = 1

oppslag = {(kort,lang) : "a",
           (lang,kort,kort,kort) : "b",
           (lang, kort, lang, kort) : "c",
           (lang, kort, kort) : "d" }


revOppslag = {}

for key, value in oppslag.items():
    revOppslag[value] = key
