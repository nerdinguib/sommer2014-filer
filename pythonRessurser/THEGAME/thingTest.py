from thing import *

def test_makeThing():
    lamp = Thing("a lamp")
    chair = Thing("a chair")
    
    assert lamp.description == "a lamp"
    assert chair.description == "a chair"

def test_thingHasWeight():
    dust = Thing("some dust")
    anvil = Thing("heavy anvil", 50)

    # standard value for weight should be 0
    assert dust.weight == 0
    assert anvil.weight == 50
