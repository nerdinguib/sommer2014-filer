#This is room.py
class Room():
    def __init__(self, desc, coords = (0,0)):
        self.description = desc
        self.coordinates = coords

    def placeNorth(self, room):
        self.north = room
        room.south = self
        roomX = self.coordinates[0]
        roomY = self.coordinates[1] - 1
        room.coordinates = (roomX, roomY)
