from player import Player
from room import Room
from thing import Thing

# room-testene og thing-testene bor loses forst

def test_canMakePlayer():
    #En spiller trenger et navn og et rom aa staa i
    room = Room("some room")
    player = Player("our hero", room)
    
    assert player.name == "our hero"
    assert player.currentRoom == room


def test_canMovePlayerNorth():
    r1 = Room("middle")
    r2 = Room("north")
    r1.placeNorth(r2)

    player = Player("dude that moves about", r1)
    player.moveNorth()
    assert player.currentRoom == r2

def test_canMovePlayerSouthAgain():
    r1 = Room("middle")
    r2 = Room("north")
    r1.placeNorth(r2)

    player = Player("dude that moves about", r1)
    player.moveNorth()
    player.moveSouth()

    assert player.currentRoom == r1

def test_moveEastAndWest():
    room = Room("middle room")
    west = Room("west room")
    east = Room("east room")


    room.placeEast(east)
    room.placeWest(west)
    player = Player("horizontal man", room)

    player.moveEast()
    assert player.currentRoom == east
    player.moveWest()
    assert player.currentRoom == room
    player.moveWest()
    assert player.currentRoom == west

def test_moveIntoWall():
    room = Room("the only room")
    player = Player("stuck forever", room)
    # Ingen av move-metodene burde flytte spilleren, men de burde ikke skape feil. 
    player.moveNorth()
    assert player.currentRoom == room
    player.moveSouth()
    assert player.currentRoom == room
    player.moveEast()
    assert player.currentRoom == room
    player.moveWest()
    assert player.currentRoom == room

def test_pickUpThing():
    room = Room("room with things")
    t1 = Thing("some thing")
    t2 = Thing("some other thing")
    room.putThing(t1)
    room.putThing(t2)

    player = Player("Picker", room)
    assert player.things == []
    player.pickUpThing(t1)
    assert player.things == [t1]
    player.pickUpThing(t2)
    assert player.things == [t1, t2]

def test_cannotPickUpThingInOtherRoom():
    r1 = Room("room player's in")
    r2 = Room("room with thing")
    thing = Thing("some thing")
    r2.putThing(thing)
    r1.placeWest(r2)
    
    player = Player("Will she find a thing?", r1)
    player.pickUpThing(thing)
    assert player.things == []

    player.moveWest()
    player.pickUpThing(thing)
    assert player.things == [thing]

def test_pickingUpThingRemovesItFromRoom():
    room = Room("room with things")
    t1 = Thing("some thing")
    t2 = Thing("some other thing")
    room.putThing(t1)
    room.putThing(t2)

    player = Player("Picker", room)
    assert room.things == [t1, t2]
    player.pickUpThing(t1)
    assert room.things == [t2]
    player.pickUpThing(t2)
    assert room.things == []

def test_canPutDownThing():
    r1 = Room("pick up here")
    r2 = Room("put down here")
    thing = Thing("move this")
    r1.putThing(thing)
    r1.placeSouth(r2)

    player = Player("carrier", r1)
    player.pickUpThing(thing)
    player.moveSouth()
    player.putDownThing(thing)

    assert r1.things == []
    assert player.things == []
    assert r2.things == [thing]

def test_canPickUpByName():
    room = Room("room with things")
    t1 = Thing("some thing")
    room.putThing(t1)
    player = Player("looks for stuff", room)
    player.pickUpThing("some thing")

    assert player.things == [t1]
    assert room.things == []

def test_canPutDownByName():
    room = Room("room with things")
    t1 = Thing("some thing")
    room.putThing(t1)
    player = Player("looks for stuff", room)
    player.pickUpThing(t1)
    
    player.putDownThing("some thing")
    assert player.things == []
    assert room.things == [t1]
