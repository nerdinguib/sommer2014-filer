#This is roomTest.py
from room import *
from thing import *

def test_canMakeRoomWithDescription():
    r = Room("red room")
    assert r.description == "red room"

def test_romHasCoordinates():
    y = Room("yellow room", (3 , 5))
    assert y.coordinates == (3 , 5)

def test_placeNorth():
    r1 = Room("middle room", (0,0))
    r2 = Room("north room")
    r1.placeNorth(r2)
    assert r1.north == r2

def test_placeNorthAlsoAddsSouth():
    r1 = Room("middle room", (0,0))
    r2 = Room("north room")
    r1.placeNorth(r2)
    assert r2.south == r1

def test_placeNorthSetsCoordinates():
    r1 = Room("middle room", (0,0))
    r2 = Room("north room")
    r1.placeNorth(r2)
    assert r2.coordinates == (0,-1)

# placeNorth testet jeg i tre omganger
# siden south, east og vest er mye det samme,
# tar jeg dem hver for seg
def test_placeSouth():
    r1 = Room("middle room", (0,0))
    r2 = Room("south room")
    r1.placeSouth(r2)
    assert r1.south == r2
    assert r2.north == r1
    assert r2.coordinates == (0,1)

def test_placeWest():
    r1 = Room("middle room", (0,0))
    r2 = Room("west room")
    r1.placeWest(r2)
    assert r1.west == r2
    assert r2.east == r1
    assert r2.coordinates == (-1,0)
    
def test_placeEast():
    r1 = Room("middle room", (0,0))
    r2 = Room("east room")
    r1.placeEast(r2)
    assert r1.east == r2
    assert r2.west == r1
    assert r2.coordinates == (1,0)

# Just to make sure...
def test_placeRoomsExtraTest():
    r1 = Room("middle room", (0,0))
    r2 = Room("right room")
    r3 = Room("more right room")
    r4 = Room("north of r3!")
    r5 = Room("north of that again!")
    r6 = Room("and some to the west")

    r1.placeEast(r2)
    r2.placeEast(r3)
    r3.placeNorth(r4)
    r4.placeNorth(r5)
    r5.placeWest(r6)

    assert r2.coordinates == (1,0)
    assert r3.coordinates == (2,0)
    assert r4.coordinates == (2,-1)
    assert r5.coordinates == (2,-2)
    assert r6.coordinates == (1,-2)

    assert r1.east.east.north.north.west == r6


#Before doing any more here, make the tests in thingTest.py work
def test_putThingInRoom():
    r1 = Room("room with thing")
    book = Thing("book")
    r1.putThing(book)
    assert book in r1.things

def test_putThingsInRoom():
    r1 = Room("room with things")
    book = Thing("book")
    lamp = Thing("lamp")
    r1.putThing(book)
    r1.putThing(lamp)
    assert book in r1.things
    assert lamp in r1.things

def test_removeThingFromRoom():
    r1 = Room("room with things")
    book = Thing("book")
    lamp = Thing("lamp")
    r1.putThing(book)
    r1.putThing(lamp)
    r1.removeThing(book)
    assert lamp in r1.things
    assert book not in r1.things

def test_removeDoesNotCauseError():
    r1 = Room("has no things")
    thing = Thing("not in r1")
    r1.removeThing(thing)

def test_addSeveralTimes():
    r1 = Room("has a thing twice? No, that's not what we want!")
    tunic = Thing("I was tired of lamps and books")
    r1.putThing(tunic)
    # Putting same thing twice!
    r1.putThing(tunic)
    # Adding an item already there should not add the thing
    assert r1.things == [tunic]

