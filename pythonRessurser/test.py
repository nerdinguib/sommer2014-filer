'''
Cheat sheet:

bind verdien 5 til navnet x
x = 5 

matte:
    addisjon er +       : 5 + 3 = 8
    subtraksjon er -    : 12 - 6 = 6
    multiplikasjon er * : 13 * 3 = 29
    divisjon er /       : 8 / 2 = 4
    rest er %           : 14 % 4 = 2 (fordi 14 = 3*4 + 2)

metodedefinisjon:
def metodeNavn(variabelNavn1, variabelNavn2, ...)
    kode!
    return returverdi

eksempel:
def addThree(x, y, z)
    return x + y + z

lister:
minListe = [12, 24, 36, 48]

minListe[0] = 12
minListe[3] = 48

hvis du gjoer:
minListe.append(60)
saa er minliste:
[12, 24, 36, 48, 60]

loekker:
for verdi in minListe:
    print(verdi)      # skriver ut 12 24 36 48

'''

from myCode import *

def test_add():
    assert add(5, 7) == 12
    assert add(-5, 5) == 0

def test_multiply():
    assert multiply(3,2) == 6
    assert multiply(15, 15) == 225

def test_addThree():
    assert addThree(2,4,9) == 15
    assert addThree(-12, 23, 11) == 22

def test_hello():
    assert hello("ola") == "hello, ola!"
    assert hello("rare mann") == "hello, rare mann!"

def test_canDrink():
    assert canDrink(10) == "nei"
    assert canDrink(18) == "ja"

def test_largest():
    assert largest(5, 6) == 6
    assert largest(6, 5) == 6
    assert largest(largest(1, 10), largest(-5, 7)) == 10

def test_sumList():
    assert sumList([5]) == 5
    assert sumList([1,2,3]) == 6
    assert sumList([545, 1434, 12, 1472]) == 3463
    assert sumList([5, 3, -7]) == 1

def test_even():
    # even: hvis tallet er et heltall, returner True
    #       hvis tallet er et oddetall returner False
    assert not even(3)
    assert even(10)
    assert even(-20)

def test_largestList():
    assert largestInList([1,2,3,4,5]) == 5
    assert largestInList([-5, 0, 5, 10]) == 10
    assert largestInList([500, 5000, 50000, 50, 5]) == 50000

def test_pythagoreanTheorem():
    # Hvis du ikke husker Pytagoras: http://www.matematikk.org/artikkel.html?tid=63132
    # Du trenger kvadratrot:
    # from math import sqrt
    # sqrt(25)
    assert pytagoras(3, 4) == 5


def test_greatestList():
    # Skal sjekke hvilken liste som har storst sum av elementer
    # Tar imot tre lister, sender tilbake LISTEN med storst sum
    assert greatestList([1],[2],[3]) == [3]
    
    list1 = [1, 2, 3, 4, 5, 6]
    list2 = [20, 30]
    list3 = [3, 5, 3, 9, 18, 7, 3, 2, 5, 9]
    assert greatestList(list1, list2, list3) == list3
    
def test_greatestArbitraryList():
    # Som greatestList, men tar inn en liste av lister.
    assert greatestAList([[5]]) == [5]
    assert greatestAList([[2,3,4],[4,5,6]]) == [4,5,6]
    assert greatestAList([[10, 20, 30], [11, 22, 33], [5, 10, 15, 20, 30]]) == [5, 10, 15, 20, 30]
    assert greatestAList([[1,2,3,4,5],[10],[22, 33], [15, 12, 18]]) == [22, 33]

    #list1 er [1, 3, 5, 7, 9, ... 99]
    list1 = []
    for x in range(0, 50):
        list1.append(x*2 + 1)
    #list2 er [2, 4, 6, 8, 10, ... 100]
    list2 = []
    for x in range(0, 50):
        list2.append((x+1)*2)
    assert greatestAList([list1, list2]) == list2
